import React, { useEffect } from "react";
import {Route, Routes} from 'react-router-dom'
import Home from './pages/Home/'
import PostDetailPage from './pages/PostDetailPage'
import CreatePostPage from './pages/CreatePost/CreatePostPage'
import { useTypedDispatch } from "./store/Store";
import { getPosts } from './store/action/posts/PostsAction';
import EditPostPage from "./pages/EditPostPage";

const App = () => {
  const dispatch = useTypedDispatch();
  useEffect(() => {
    const init = async () => {
      dispatch(getPosts("/api/newsposts"));
    };
    init();
  }, [dispatch]);
  return (
    <Routes>
      <Route path="/" element={<Home />}/>
      <Route path="/newsposts/create" element={<CreatePostPage />}/>
      <Route path="/newsposts/:id" element={<PostDetailPage />}/>
      <Route path="/newsposts/:id/edit" element={<EditPostPage />}/>
      <Route element={<h1>Not Found</h1>}/>
    </Routes>
  )
}
export default App