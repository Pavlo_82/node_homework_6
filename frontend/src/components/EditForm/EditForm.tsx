import React, { useEffect } from "react";
import { Formik, Form, Field, FormikHelpers, FormikErrors } from "formik";
import Button from "../Button";
import * as Yup from "yup";
import { getPostById, updateAPost } from "../../store/action/posts/PostsAction";
import { RootStore, useTypedDispatch } from "../../store/Store";
import { useNavigate, useParams } from "react-router-dom";
import { useSelector } from "react-redux";
interface UpdatedValues {
  title: string;
  text: string;
}

const EditForm: React.FC = () => {
  const dispatch = useTypedDispatch()
  const navigate = useNavigate();
  const { id } = useParams();
  const post = useSelector((state: RootStore) => state.posts?.post);

  const initialValues: UpdatedValues = { title: post.title, text: post.text };

  useEffect(() => {
    if (id) {
      dispatch(getPostById("/api/newsposts/", id));
    }
  }, [id, dispatch]);
  const handleSubmit = async (values: UpdatedValues, actions:FormikHelpers<UpdatedValues>) => {    
    try {
      const PostSchema = Yup.object().shape({
        title: Yup.string()
          .min(2, "Too Short!")
          .max(50, "Too Long!")
          .required("Title is required"),
      
        text: Yup.string()
          .min(2, "Too Short!")
          .max(1000, "Too Long!")
          .required("Post is required"),
      });
      await PostSchema.validate(values, { abortEarly: false });
      const updatedId = await dispatch(updateAPost('/api/newsposts/', values, post.id))
      actions.setSubmitting(false);
      navigate(`/newsposts/${updatedId}`);
    } catch (errors) {
      if (Yup.ValidationError.isError(errors)) {
        const formErrors: FormikErrors<any> = {};
        errors.inner.forEach((error) => {
          if (typeof error.path === 'string') {
            formErrors[error.path] = error.message;
          }
        });
        actions.setErrors(formErrors);
      }
    }
  };
  return (
    <div className="max-w-[500px] mx-auto my-[20px]">
    <Formik
      initialValues={initialValues}
      onSubmit={handleSubmit}
    >
      <Form className="flex flex-col">
        <div className="flex flex-col">
          <label htmlFor="title">Title</label>
          <Field id="title" name="title" placeholder="Title" />
        </div>
        <div className="flex flex-col">
          <label htmlFor="text">Post</label>
          <Field id="text" name="text" placeholder="text" />
        </div>
        <Button id='updatePost'/>
      </Form>
    </Formik>
    </div>
  );
}

export default EditForm