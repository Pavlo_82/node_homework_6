import PostsRepository from '../dal/posts/posts.repository';
export interface Params {
  size: number | null;
  page: number | null;
  filter: any;
}

export interface IErrors {
  statusCode: number;
  message: string;
}


export interface INews {
  id?: string;
  title: string;
  text: string;
}

export interface FieldSchema {
  type: 'string' | 'number' | 'boolean';
  required?: boolean;
}

export interface ISchema {
  [key: string]: FieldSchema;
}

export interface ITable {
  getAllPosts(): any;
  getById(id: string): any;
  createAPost(data: INews): any;
  update(id: string, data: INews): any;
  delete(id: string): any;
}

export interface IRegisterSchema {
  registerSchema(tablesname: string, schema: ISchema): void;
  getTable(schemaName: string): PostsRepository;
}

export interface IError {
  message: string;
}
