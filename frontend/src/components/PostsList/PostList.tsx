import React from 'react'
import { RootStore, useTypedSelector } from '../../store/Store';
import PostCard from './PostCard'
import { PostType } from '../../store/action/posts/PostsActionTypes';

const PostList: React.FC = () => {
  const postList = useTypedSelector((state: RootStore) => Object.values(state.posts?.posts ?? {}))
  return (
    <ul className='grid grid-cols-3 gap-5'>
     { postList.map((post:PostType)=>(
        <PostCard key={post.id} id={post.id}/>
     )) }
    </ul>
  )
}

export default PostList