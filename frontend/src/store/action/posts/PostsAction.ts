import { Dispatch } from "redux";
import {
  PostsDispatchTypes,
  IPostType,
  POSTS_LOADING,
  POSTS_FAIL,
  POSTS_SUCCESS,
  POST_CREATED,
  POST_DELETE_ONE,
  POST_EDIT,
  POST_SUCCESS,
} from "./PostsActionTypes";
import axios from "axios";
import { TypedThunkAction } from "../../Store";

export const getPosts =
  (url: string):TypedThunkAction => async (dispatch, getState) => {
    try {
      dispatch({
        type: POSTS_LOADING,
      });
      const res = await axios.get(url);
      dispatch({
        type: POSTS_SUCCESS,
        payload: res.data.posts,
      });
    } catch (e) {
      dispatch({
        type: POSTS_FAIL,
      });
    }
  };

export const deletePost =
  (url: string, id:string) => async (dispatch: Dispatch<PostsDispatchTypes>) => {
    try {
      const res = await axios.delete(`${url}${id}`);
      if(!res){
        throw new Error(res)
      }
      dispatch({
        type: POST_DELETE_ONE,
        payload: id,
      });
    } catch (e) {
      dispatch({
        type: POSTS_FAIL,
      });
    }
  };

  export const getPostById =
  (url: string, id:string) => async (dispatch: Dispatch<PostsDispatchTypes>) => {
    try {
      const res = await axios.get(`${url}${id}`);
      dispatch({
        type: POST_SUCCESS,
        payload: res.data,
      });
    } catch (e) {
      dispatch({
        type: POSTS_FAIL,
      });
    }
  };

  export const createAPost =
  (url: string, post:IPostType) => async (dispatch: Dispatch<PostsDispatchTypes>) => {
    try {
      const res = await axios.post(`${url}`, post);
      dispatch({
        type: POST_CREATED,
        payload: res.data,
      });      
      return res.data.id
    } catch (e) {
      dispatch({
        type: POSTS_FAIL,
      });
    }
  };

  export const updateAPost =
  (url: string, post:IPostType, id: string) => async (dispatch: Dispatch<PostsDispatchTypes>) => {
    try {
      const res = await axios.put(`${url}${id}`, post);
      dispatch({
        type: POST_EDIT,
        payload: res.data,
      });
      console.log(res.data.id);
            
      return res.data.id
    } catch (e) {
      dispatch({
        type: POSTS_FAIL,
      });
    }
  };
