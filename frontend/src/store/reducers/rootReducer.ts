import { combineReducers } from "redux";
import PostsReducer from "./postReducer";
import ModalReducer from "./modalReducer";

//combine reducers
const rootReducer = combineReducers({
    posts: PostsReducer,
    modal: ModalReducer
});

export default rootReducer;