import {
  PostType,
  PostsDispatchTypes,
  POSTS_LOADING,
  POSTS_FAIL,
  POSTS_SUCCESS,
  POST_CREATED,
  POST_DELETE_ONE,
  POST_EDIT,
  POST_SUCCESS,
} from "../action/posts/PostsActionTypes";

interface IInitialState {
  loading?: boolean;
  posts?: { [key: string]: PostType };
  id?: string;
  post: PostType;
}

interface IAcc {
  [key: string]: PostType;
}

//initial state
const initialState: IInitialState = {
  loading: true,
  posts: {},
  post: { id: "", title: "", text: "" },
};

//note reducer
const PostsReducer = (
  state: IInitialState = initialState,
  action: PostsDispatchTypes
) => {
  switch (action.type) {
    case POSTS_SUCCESS:
      const result = action.payload.reduce((acc: IAcc, cur: PostType) => {
        acc[cur.id] = cur;
        return acc;
      }, {});
      return { ...state, posts: result, loading: false };
    case POST_DELETE_ONE:
      const deletedState = { ...state };
      console.log(action.payload);
      if (action.payload) {
        const postIdToDelete = action.payload;
        if (
          deletedState.posts &&
          deletedState.posts.hasOwnProperty(postIdToDelete)
        ) {
          delete deletedState.posts[postIdToDelete];
        }
      }
      return deletedState;
    case POST_SUCCESS:
      return { ...state, post: action.payload, loading: false };
    case POST_CREATED:
      const newState = { ...state };
      if (newState.posts) {
        newState.posts[action.payload.id] = action.payload;
        return newState;
      }
      return newState;
    case POST_EDIT:
      const updatedState = { ...state };
      if (updatedState.posts) {
        updatedState.posts[action.payload.id] = action.payload;
        return updatedState;
      }
      return updatedState;
    default:
      return state;
  }
};

export default PostsReducer;
