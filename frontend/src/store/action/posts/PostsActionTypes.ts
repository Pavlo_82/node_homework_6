//types
export const POSTS_LOADING = "POSTS_LOADING";
export const POSTS_FAIL = "POSTS_FAIL";
export const POSTS_SUCCESS = "POSTS_SUCCESS";
export const POST_CREATED = "POST_CREATED";
export const POST_DELETE_ONE = "POST_DELETE_ONE";
export const POST_EDIT = "POST_EDIT";
export const POST_SUCCESS = "POST_SUCCESS";

//interfaces
export type PostType = {
  text: string;
  title: string;
  id: string;
};

export type IPostType = {
  text: string;
  title: string;
};

export interface PostsLoading {
  type: typeof POSTS_LOADING;
}

export interface PostsFail {
  type: typeof POSTS_FAIL;
}

export interface PostsSuccess {
  type: typeof POSTS_SUCCESS;
  payload: Array<PostType>;
}

export interface PostCreate {
  type: typeof POST_CREATED;
  payload: PostType;
}

export interface PostDeleteOne {
  type: typeof POST_DELETE_ONE;
  payload: string;
}

export interface PostEdit {
  type: typeof POST_EDIT;
  payload: PostType;
}

export interface PostOneSuccess {
  type: typeof POST_SUCCESS;
  payload: PostType;
}
//All types actions
export type PostsDispatchTypes =
  | PostsLoading
  | PostsFail
  | PostsSuccess
  | PostCreate
  | PostDeleteOne
  | PostEdit
  | PostOneSuccess;
