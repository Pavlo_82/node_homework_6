import React, { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { RootStore, useTypedDispatch } from "../../store/Store";
import { getPostById } from "../../store/action/posts/PostsAction";
import { NavLink } from "react-router-dom";

const PostDetailPage: React.FC = () => {
  const { id } = useParams();
  const dispatch = useTypedDispatch();
  const post = useSelector((state: RootStore) => state.posts?.post);
  useEffect(() => {
    if (id) {
      dispatch(getPostById("/api/newsposts/", id));
    }
  }, [id, dispatch]);
  return (
    <div className="max-w-[1200px] mx-auto my-[20px]">
      <NavLink className='bg-red-600 rounded border-none py-2 px-5 mt-3 inline-block' to='/'>Home</NavLink>
      <h2>{post.title}</h2>
      <p>{post.text}</p>
      <NavLink className='bg-red-600 rounded border-none py-2 px-5 mt-3 inline-block' to={`/newsposts/${post.id}/edit`}>Update</NavLink>
    </div>
  );
};

export default PostDetailPage;
