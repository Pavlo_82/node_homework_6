import PostsService from '../../bll/posts/posts.service.ts';
import express from 'express';
import { INews } from 'types/params.interface.ts';
import {Service} from 'typedi';

@Service()
class PostsController {
  public path = '/newsposts';
  public router = express.Router();

  constructor(private postsService: PostsService) {
    this.initializeRoutes();
  }

  public initializeRoutes() {
    this.router.get('/', this.getAllPosts);
    this.router.get('/:id', this.getPostById);
    this.router.post('/', this.createAPost);
    this.router.put('/:id', this.updateAPost);
    this.router.delete('/:id', this.deleteAPost);
  }

  getPostById = async (request: express.Request, response: express.Response) => {
    const id:string = request.params.id
    const post = await this.postsService.getById(id); 
    response.send(post);
  }

  getAllPosts = async (request: express.Request, response: express.Response) => {
    const params = {
      size: request.query.size ? Number(request.query.size) : null,
      page: request.query.page ? Number(request.query.page) : null,
      filter: request.query.filter || {},
    };
    const pagedPosts = await this.postsService.getAllPosts(params); 
    response.send(pagedPosts);
  };

  createAPost = async (request: express.Request, response: express.Response) => {
    const post: INews = request.body;
    try {
      const createdPost = await this.postsService.createAPost(post);
      response.send(createdPost);
    } catch (e:any) {
      throw new Error(e);
    }
  };

  updateAPost = async (request: express.Request, response: express.Response) => {
    const post: INews = request.body;
    const id:string = request.params.id
    console.log(request.params.id);
    
    try {
      const updatedPost = await this.postsService.updateAPost(id, post);
      response.send(updatedPost);
    } catch (e:any) {
      throw new Error(e);
    }
  };

  deleteAPost = async (request: express.Request, response: express.Response) => {
    const id:string = request.params.id
    try {
      const message = await this.postsService.deleteAPost(id);
      response.send(message);
    } catch (e:any) {
      throw new Error(e);
    }
  };
}

export default PostsController;
