import React from "react";
import { RootStore, useTypedDispatch, useTypedSelector } from "../../store/Store";
import { deletePost } from "../../store/action/posts/PostsAction";
import Button from "../Button";
import { NavLink } from "react-router-dom";
interface IPostCard {
  id: string;
}

const PostCard: React.FC<IPostCard> = ({ id }) => {
  const dispatch = useTypedDispatch();
  const post = useTypedSelector((state: RootStore) => {
    if (state.posts && state.posts.posts && state.posts.posts[id]) {
      return state.posts.posts[id];
    }
    return {title: '', text:''}; // Provide a default value or handle the case when 'post' is not found
  });
  const handleDelete = (id:string) => {
    dispatch(deletePost('/api/newsposts/', id))
  } 
  return (
    <li className="flex flex-col gap-5 rounded shadow-2xl p-5 shadow-black border-2 border-solid">
      <h2>Title: {post.title}</h2>
      <p>Post: {post.text}</p>
      <NavLink className='bg-red-600 rounded border-none py-2 px-5' to={`/newsposts/${id}`}>Show Details</NavLink>
      <Button id='delete' onClick={()=>handleDelete(id)}/>
    </li>
  );
};

export default PostCard;
