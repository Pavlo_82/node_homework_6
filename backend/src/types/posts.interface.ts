export interface Post {
  author: string;
  content: string;
  title: string;
}

export interface PagedPosts {
  posts: Post[];
  total: number;
  size: number;
  page: number;
}
