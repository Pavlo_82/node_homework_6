import { PagedPosts } from '../../types/posts.interface.ts';
import {IError, INews, ITable, Params} from '../../types/params.interface.ts';
import { Service } from 'typedi';
import { FileDB } from '../../fileDb/fileDb.ts';

@Service()
class PostsRepository {
  private table: ITable
  constructor(){
    const fileDBInstance = FileDB.getInstance();
    this.table = fileDBInstance.getTable('db');
  }
  getAllPosts = async (params: Params): Promise<PagedPosts> => {
    const array = await this.table.getAllPosts()
   return {
      total: array.length,
      posts: array,
      size: params.size || 5,
      page: params.page || 1,
   };
  };

  getById = async(id:string):Promise<IError | INews> => {
    const post = await this.table.getById(id)    
    return await post;
  }

  createAPost = async (post: INews):Promise<INews> => {
    const newPost = await this.table.createAPost(post)    
    return await newPost;
  };

  updateAPost = async (id:string, post: INews):Promise<IError | INews> => {
    const newPost = await this.table.update(id, post)    
    return await newPost;
  };

  deleteAPost= async (id:string):Promise<IError | INews> => {
    const newPost = await this.table.delete(id)    
    return await newPost;
  };
}

export default PostsRepository;
