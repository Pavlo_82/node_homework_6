import PostsRepository from '../../dal/posts/posts.repository.ts';
import {IError, INews, Params} from '../../types/params.interface.ts';
import {Service} from 'typedi';
import { PagedPosts } from 'types/posts.interface.ts';

@Service()
class PostsService {
  
  constructor(private postsRepository:PostsRepository) {
    
  }

  getAllPosts = async(params: Params): Promise<PagedPosts> => {   
    return await this.postsRepository.getAllPosts(params);
  };

  getById = async(id:string):Promise<IError | INews> => {
    const post = await this.postsRepository.getById(id)    
    return await post;
  }

  createAPost = async (post: INews): Promise<INews> => {  
    return await this.postsRepository.createAPost(post);
  };

  updateAPost = async (id:string, post: INews): Promise<IError | INews> => {  
    return await this.postsRepository.updateAPost(id, post);
  };

  deleteAPost = async (id:string): Promise<IError | INews> => {  
    return await this.postsRepository.deleteAPost(id);
  };
}

export default PostsService;
